all: test

test: test_main.o test_sort.o test_search.o search.o sort.o
	g++ -g -O0 -std=c++20 -o $@ $^

%.o: %.cpp
	g++ -g -O0 -std=c++20 -o $@ -c $<

test_sort.o: test_main.cpp sort.cpp
test_search.o: test_main.cpp search.cpp

.PHONY: clean run

clean:
	rm *.o test

run:
	./test
