#include <vector>
#include <chrono>

namespace sort
{
    using ms = std::chrono::milliseconds;
    struct info
    {
        const char *name;
        unsigned long elements;
        unsigned assignment;
        unsigned comparison;
        ms time;
    };

    info bubble(std::vector<int>&);
    info bubble_shaker(std::vector<int>&);
    info insertion(std::vector<int>&);
    info insertion_shift(std::vector<int>&);
    info insertion_binary(std::vector<int>&);
}
