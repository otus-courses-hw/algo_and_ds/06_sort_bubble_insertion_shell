#include "search.hpp"
#include <iterator>

namespace search
{
    namespace {
        template <typename T>
        using Iter = typename std::vector<T>::const_iterator;

        template <typename T>
        auto helper(T element, Iter<T> left, Iter<T> right)
        {
            if (left >= right)
            {
                if (element >= *left)
                    return std::next(left);
                else
                    return left;
            }

            if (auto mid = std::next(left, std::distance(left, right) / 2); element >= *mid)
            {
                return helper(element, std::next(mid), right);
            }
            else
            {
                return helper(element, left, std::prev(mid));
            }
        }
    }

    index binary(int element, unsigned left, unsigned right, const std::vector<decltype(element)> &vec)
    {
        auto begin = std::next(vec.begin(), left);
        auto end = std::next(vec.begin(), right);

        auto pos = helper(element, begin, end);

        return std::distance(vec.begin(), pos);
    }
}