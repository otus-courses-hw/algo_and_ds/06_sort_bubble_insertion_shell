#include <catch2/catch.hpp>

#include "sort.hpp"

#include <random>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <chrono>
#include <functional>

namespace ranges = std::ranges;

auto measure(std::vector<int> &data, const std::function<sort::info(decltype(data))> &sort)
{
    const auto start = std::chrono::steady_clock::now();
    auto info = sort(data);
    const auto end = std::chrono::steady_clock::now();
    info.time = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    return info;
}

auto dice(int n)
{
    static std::random_device rd;
    static std::mt19937 gen(rd());
    std::uniform_int_distribution<> distrib(0, n * (int) std::log(n));
    return distrib(gen);
}

std::vector<int> randomize(int size)
{
    std::vector<int> vec(size);
    ranges::generate(vec, [size](){return dice(size);});
    return vec;
}

void print(sort::info &i)
{
    std::cout
    << std::setw(15) << i.algo << " "
    << std::setw(15) << "N: "    << std::setw(15) << i.elements << " "
    << std::setw(15) << "cmp: "  << std::setw(15) << i.comparison << " "
    << std::setw(15) << "asg: "  << std::setw(15) << i.assignment << " "
    << std::setw(15) << "ms: "   << std::setw(15) << i.time.count() << std::endl;
}

TEST_CASE("bubble", "[sort]")
{
    SECTION("100")
    {
        auto vec = randomize(100);
        auto info = measure(vec, sort::bubble);
        print(info);
    }
    SECTION("1000")
    {
        auto vec = randomize(1000);
        auto info = measure(vec, sort::bubble);
        print(info);
    }
    SECTION("10'000")
    {
        auto vec = randomize(10'000);
        auto info = measure(vec, sort::bubble);
        print(info);
    }
    SECTION("100'000")
    {
        auto vec = randomize(100'000);
        auto info = measure(vec, sort::bubble);
        print(info);
    }
}

TEST_CASE("bubble_shaker", "[sort]")
{
    SECTION("100")
    {
        auto vec = randomize(100);
        auto info = measure(vec, sort::bubble_shaker);
        print(info);
    }
    SECTION("1000")
    {
        auto vec = randomize(1000);
        auto info = measure(vec, sort::bubble_shaker);
        print(info);
    }
    SECTION("10'000")
    {
        auto vec = randomize(10'000);
        auto info = measure(vec, sort::bubble_shaker);
        print(info);
    }
    SECTION("100'000")
    {
        auto vec = randomize(100'000);
        auto info = measure(vec, sort::bubble_shaker);
        print(info);
    }
}

TEST_CASE("insertion", "[sort]")
{
    SECTION("100")
    {
        auto vec = randomize(100);
        auto info = measure(vec, sort::insertion);
        print(info);
    }
    SECTION("1000")
    {
        auto vec = randomize(1000);
        auto info = measure(vec, sort::insertion);
        print(info);
    }
    SECTION("10'000")
    {
        auto vec = randomize(10'000);
        auto info = measure(vec, sort::insertion);
        print(info);
    }
    SECTION("100'000")
    {
        auto vec = randomize(100'000);
        auto info = measure(vec, sort::insertion);
        print(info);
    }
}

TEST_CASE("insertion_shift", "[sort]")
{
    SECTION("100")
    {
        auto vec = randomize(100);
        auto info = measure(vec, sort::insertion_shift);
        print(info);
    }
    SECTION("1000")
    {
        auto vec = randomize(1000);
        auto info = measure(vec, sort::insertion_shift);
        print(info);
    }
    SECTION("10'000")
    {
        auto vec = randomize(10'000);
        auto info = measure(vec, sort::insertion_shift);
        print(info);
    }
    SECTION("100'000")
    {
        auto vec = randomize(100'000);
        auto info = measure(vec, sort::insertion_shift);
        print(info);
    }
}

TEST_CASE("insertion_binary", "[sort]")
{
    SECTION("100")
    {
        auto vec = randomize(100);
        auto info = measure(vec, sort::insertion_binary);
        print(info);
    }
    SECTION("1000")
    {
        auto vec = randomize(1000);
        auto info = measure(vec, sort::insertion_binary);
        print(info);
    }
    SECTION("10'000")
    {
        auto vec = randomize(10'000);
        auto info = measure(vec, sort::insertion_binary);
        print(info);
    }
    SECTION("100'000")
    {
        auto vec = randomize(100'000);
        auto info = measure(vec, sort::insertion_binary);
        print(info);
    }
}
