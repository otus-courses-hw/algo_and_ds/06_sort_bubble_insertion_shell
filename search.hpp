#include <vector>

namespace search
{
    using index = std::vector<int>::difference_type;

    //search a position where to insert an element
    index binary(int, unsigned, unsigned, const std::vector<int> &);
}