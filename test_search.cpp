#include <catch2/catch.hpp>

#include "search.hpp"

TEST_CASE("happy_pass", "[search]")
{
    std::vector<int> v{0,1,2,3,4,5};
    auto from = 0U;
    auto to = v.size() - 1;

    for (auto i = 0U; i < v.size(); ++i)
    {
        auto pos = search::binary(i, from, to, v);
        CHECK(pos == i + 1);
    }
}

TEST_CASE("there are equals in a row", "[search]")
{
    auto from = 0U;
    SECTION("in the middle")
    {
        std::vector<int> v{0,1,2,2,2,3};
        auto pos = search::binary(2, from, v.size() - 1, v);
        CHECK(pos == v.size() - 1);
    }
    SECTION("in the beginning")
    {
        std::vector<int> v{0,0,1,2,2,3};
        auto pos = search::binary(0, from, v.size() - 1, v);
        CHECK(pos == 2);
    }
    SECTION("in the end")
    {
        std::vector<int> v{0,1,2,3,4,4};
        auto pos = search::binary(4, from, v.size() - 1, v);
        CHECK(pos == v.size());
    }
}

TEST_CASE("there is not an element in the container", "[search]")
{
    std::vector<int> v {1,2,4,4,5};
    auto from = 0U;
    auto to = v.size() - 1;

    SECTION("greater than all")
    {
        auto pos = search::binary(6, from, to, v);
        CHECK(pos == v.size());
    }
    SECTION("somewhere in the middle")
    {
        auto pos = search::binary(3, from, to, v);
        CHECK(pos == 2);
    }
    SECTION("less than all")
    {
        auto pos = search::binary(0, from, to, v);
        CHECK(pos == 0);
    }
}