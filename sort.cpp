#include "sort.hpp"
#include "search.hpp"
#include <utility>

namespace sort
{
    namespace {
        info algo_stat;

        auto gt(const int lhs, const int rhs)
        {
            algo_stat.comparison++;
            return lhs > rhs;
        }

        auto lt(const int lhs, const int rhs)
        {
            algo_stat.comparison++;
            return lhs < rhs;
        }
    }

    info bubble(std::vector<int> &vec)
    {
        algo_stat.name = "bubble";
        algo_stat.elements = vec.size();
        algo_stat.comparison = 0;
        algo_stat.assignment = 0;

        for (auto j = vec.size()-1; j > 0; --j)
        {
            auto was_swapping = false;
            for (auto i = 0UL; i < j; ++i)
            {
                if (gt(vec[i], vec[i+1]))
                {
                    std::swap(vec[i], vec[i+1]); algo_stat.assignment += 3;
                    was_swapping = true;
                }
            }

           if (!was_swapping)
               break;
        }

        return algo_stat;
    }

    info bubble_shaker(std::vector<int> &vec)
    {
        algo_stat.name = "bubble_shaker";
        algo_stat.elements = vec.size();
        algo_stat.comparison = 0;
        algo_stat.assignment = 0;

        auto left = 0UL;
        auto right = vec.size() - 1;

        while (left <= right)
        {
            for (auto i = left; i < right; ++i)
            {
                if (gt(vec[i], vec[i+1]))
                {
                    std::swap(vec[i], vec[i+1]);
                    algo_stat.assignment += 3;
                }
            }
            --right;

            for (auto i = right; i > left; --i)
            {
                if (lt(vec[i], vec[i-1]))
                {
                    std::swap(vec[i], vec[i-1]);
                    algo_stat.assignment += 3;
                }
            }
            ++left;
        }
        return algo_stat;
    }

    info insertion(std::vector<int> &vec)
    {
        algo_stat.name = "insertion";
        algo_stat.elements = vec.size();
        algo_stat.comparison = 0;
        algo_stat.assignment = 0;


        for (auto j = 1UL; j < vec.size(); ++j)
            for (auto i = j-1; i >= 0 && gt(vec[i], vec[i+1]); --i)
            {
                std::swap(vec[i+i], vec[i]);
                algo_stat.assignment += 3;
            }

        return algo_stat;
    }

    info insertion_shift(std::vector<int> &vec)
    {
        algo_stat.name = "insertion_shift";
        algo_stat.elements = vec.size();
        algo_stat.comparison = 0;
        algo_stat.assignment = 0;

        for (auto j = 1UL; j < vec.size(); ++j)
        {
            auto saved = vec[j]; algo_stat.assignment++;

            auto i = j - 1;
            for (; i >= 0 && gt(vec[i], saved); --i)
            {
                vec[i+1] = vec[i];
                algo_stat.assignment++;
            }

            vec[i+1] = saved; algo_stat.assignment++;
        }
        return algo_stat;
    }

    info insertion_binary(std::vector<int> &vec)
    {
        algo_stat.name = "insertion_binary";
        algo_stat.elements = vec.size();
        algo_stat.comparison = 0;
        algo_stat.assignment = 0;

        for (int j = 1; j < vec.size(); ++j)
        {
            auto saved = vec[j]; algo_stat.assignment++;
            auto pos = search::binary(saved, 0, j - 1, vec); algo_stat.assignment++;

            auto i = j - 1;
            for (; i >= pos; i--)
            {
                vec[i+1] = vec[i]; algo_stat.assignment++;
            }

            vec[i+1] = saved; algo_stat.assignment++;
        }
        return algo_stat;
    }
}
